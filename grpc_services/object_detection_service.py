import numpy as np
from utils import array_byte_converter as tools
import grpc
from grpc_services import object_detection_pb2_grpc
from grpc_services import object_detection_pb2
import time

class ObjectDetectionService(object_detection_pb2_grpc.ObjectDetectionProcessingServiceServicer):
    def __init__(self, **kwargs):
        # Get object detector
        self.object_detector = kwargs.get('object_detector')()

    def ObjectDetectionProcess(self, request, context):
        result = None
        try:
            image = self.parse_data(request)
            result = self.process_image(image)
        except:
            print('[ERROR] Cannot handle request')
        return result


    def process_image(self, image):
        # Get the output from YOLO
        bbxs, labels, _ = self.object_detector.predict(image)
        # Package the output to ObjectDetectResult defined in object_detection.proto file
        result = self.package_results(bbxs, labels)
        return result

    def package_results(self, bbxs, labels):
        bbxs_bytes, bbxs_shape, bbxs_type = None, None, None
        try:
            # Serialize bbxs from numpy type to bytes
            bbxs_bytes, bbxs_shape, bbxs_type = tools.np_array_to_bytes(bbxs)
        except:
            print('[ERROR] When packaging results')
        return object_detection_pb2.ObjectDetectResult(objects_bbs_bytes=bbxs_bytes, objects_bbs_shape=bbxs_shape, objects_bbs_type=bbxs_type, labels=labels)

    def parse_data(self, request):
        # Get images from image bytes, shape and type
        image_bytes, image_shape, image_type = request.Image_bytes, request.Image_shape, request.Image_type
        image = tools.bytes_to_np_array(image_bytes, image_shape, image_type)
        return image
