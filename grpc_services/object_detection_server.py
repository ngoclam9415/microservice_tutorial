from grpc_services import object_detection_pb2_grpc
from grpc_services import object_detection_pb2
from grpc_services import object_detection_service
from core.yolo import YoloObjectDetector
from abc import ABC, abstractmethod
import grpc
from concurrent import futures
import time
import argparse


_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class BaseServer(ABC):
    def __init__(self, **kwargs):
        port = kwargs.get('port', 1234) # Get port
        max_thread_workers = kwargs.get('max_thread_workers', 1) # Get number of thread 
        self.address = '[::]:%s'%(port) # Get localhost address
        # Init grpc server
        # max_workers chi ra so luong thread nhan request
        # grpc.max_send_message_length, grpc.max_receive_message_length bang thong toi da cho 1 message
        # maximum_concurrent_rpcs bao nhieu request co the nhan cung 1 luc boi 1 thread workers
        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=max_thread_workers),
                                    options=[('grpc.max_send_message_length', 1024 * 1024 * 1024),
                                            ('grpc.max_receive_message_length', 1024 * 1024 * 1024)],
                                            maximum_concurrent_rpcs=6)

    @abstractmethod
    def add_services(self):
        # Add service vo server
        pass

    def run(self):
        self.add_services()
        self.server.add_insecure_port(self.address) # add port nhan request
        self.server.start() # start server
        print('Server is online at port', self.address)
        # add sleep to keep server running
        try:
            while True:
                time.sleep(_ONE_DAY_IN_SECONDS)
        except KeyboardInterrupt:
            self.server.stop(0)

class YoloObjectDetectionServer(BaseServer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # Khai bao kieu object detector
        self.object_detector = YoloObjectDetector

    def add_services(self):
        # add service vao server
        object_detection_pb2_grpc.add_ObjectDetectionProcessingServiceServicer_to_server(
            object_detection_service.ObjectDetectionService(object_detector=self.object_detector), 
            server=self.server
        )


if __name__=='__main__':
    parser = argparse.ArgumentParser('Object Detector',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--port', type=int, help='port', default=8887)
    parser.add_argument('-t', '--max_thread_workers', type=int, help='number of worker', default=6)
    args = parser.parse_args()
    server = YoloObjectDetectionServer(max_thread_workers=args.max_thread_workers, port=args.port)
    server.run()