import numpy as np
import grpc
from grpc_services import object_detection_pb2
from grpc_services import object_detection_pb2_grpc
import time
from utils import array_byte_converter as tools

class YoloObjectDetectionClient:
    def __init__(self, address):
        self.address = address # Address ma server dang chay
        # Thiet lap connection khong co authenticate
        self.channel = grpc.insecure_channel(self.address) 
        self.stub = object_detection_pb2_grpc.ObjectDetectionProcessingServiceStub(self.channel)

    def predict(self, image):
        bbxs, labels = [], []
        try:
            response = self.send_request(image)
            bbxs, labels = self.parse_response(response)
        except grpc.RpcError as e:
            print('GRPC ERROR : ',e)
        return bbxs, labels

    def send_request(self, image):
        # Serialize du lieu roi truyen qua dang input ArrayImage da duoc dinh nghia trong file proto
        image_bytes, image_shape, image_type = \
            tools.np_array_to_bytes(image)
        response = self.stub.ObjectDetectionProcess(
            object_detection_pb2.ArrayImage(Image_bytes=image_bytes, 
                                            Image_shape=image_shape,
                                            Image_type=image_type)
        )
        return response

    def parse_response(self, response):
        # response la dang ObjectDetectResult da duoc dinh nghia trong file proto
        objects_bbs_bytes, objects_bbs_shape, objects_bbs_type = \
            response.objects_bbs_bytes, response.objects_bbs_shape, response.objects_bbs_type
        labels = response.labels
        # Deserialize boundind boxes tu byte qua np array
        bbxs = tools.bytes_to_np_array(objects_bbs_bytes, objects_bbs_shape, objects_bbs_type)
        return bbxs, labels


if __name__ == '__main__':
    import cv2
    import numpy as np
    object_detector = YoloObjectDetectionClient(address='localhost:8887')
    img = cv2.imread('/home/ngoclam9415/Downloads/people.jpeg')
    bbxs, labels = object_detector.predict(img)
    for (x1, y1, x2, y2) in bbxs:
        cv2.rectangle(img, (x1, y1), (x2, y2), (0, 255, 0), 2 ,1)
    print(bbxs)
    cv2.imshow('img', img)
    cv2.waitKey(0)