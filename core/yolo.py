import cv2
import gluoncv as gcv
import mxnet as mx
from gluoncv import model_zoo, data, utils
import numpy as np

class YoloObjectDetector:

    def __init__(self):
        # self.gpu_device = mx.gpu()
        self.gpu_device = mx.cpu()
        self.net = model_zoo.get_model('yolo3_darknet53_coco', pretrained=True, ctx=self.gpu_device)
        
    def predict(self, image, nms_thresh=0.2, score_thresh=0.6):
        self.net.set_nms(nms_thresh=nms_thresh) # Set non max suppression for our model
        if image is not None and image.size: # Catch if input is blank image
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) # Convert image from BGR to RGB
            mxnet_NDarray = mx.nd.array(image) # Convert it to mxnet array type
            # Reshape mxnet array to have min size of 512, x is mxnet array, img is np array for visualization
            x, img = gcv.data.transforms.presets.yolo.transform_test(mxnet_NDarray, short=512)
            x = x.as_in_context(context=self.gpu_device) # Load mxnet array to GPU
            class_IDs, scores, bounding_boxs = self.net(x) # Predict output
            # Convert sorces and class_Ids to 1-D array
            scores = scores.asnumpy().flatten()
            class_IDs = class_IDs.asnumpy().flatten()
            # Convert BBs to 2-D array
            bounding_boxs = bounding_boxs.asnumpy().reshape(*bounding_boxs.asnumpy().shape[-2:])
            # Reshape output back to it original size
            scale_factor = [image.shape[1]/img.shape[1],
                            image.shape[0]/img.shape[0],
                            image.shape[1] / img.shape[1],
                            image.shape[0] / img.shape[0]]
            bounding_boxs = bounding_boxs*scale_factor
            return_boudingboxes = []
            return_class_IDs = []
            return_scores = []
            # Filter boxes that are not person or score < score_thresh
            for (count,i) in enumerate(class_IDs.tolist()):
                if self.net.classes[int(i)] !='person' or scores[count] < score_thresh:
                    continue
                return_boudingboxes.append(bounding_boxs[count].astype(np.int32).tolist())
                return_class_IDs.append(self.net.classes[int(i)])
                return_scores.append(scores[count])
        else:
            return [], [], []
        return np.array(return_boudingboxes), return_class_IDs, return_scores

    def to_tlwh(self, bounding_boxs):
        bounding_boxs = np.array(bounding_boxs)
        if len(bounding_boxs):
            bounding_boxs[:,2:] = bounding_boxs[:,2:] - bounding_boxs[:,:2]
            return bounding_boxs.astype(np.int)
        else:
            return bounding_boxs
